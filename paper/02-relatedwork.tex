% !TEX root = ./www2019-fp-yujie-pengjie.tex
\section{Related Work}
\begin{figure*}
 \centering
 \subfigure{
 \includegraphics[width=1.0\textwidth]{fig/3-1.eps}}
 \caption{Overview of \ac{FARM}. The fashion generator (top) uses a variational transformer to learn a special Gaussian distribution for a given top image $\textbf{I}_t$ and a given bottom description $\textbf{d}$. It then generates a bottom image $\textbf{I}_g$ to match $\textbf{I}_t$ and $\textbf{d}$. The fashion recommender (bottom) evaluates the matching score between the recommended bottom image $\textbf{I}_b$ and $(\textbf{I}_t,\textbf{d})$ pair from three angles, i.e., visual matching, description matching, and layer-to-layer matching.}
  \label{f_3_1}
\end{figure*}

We survey related work on fashion recommendation by focusing on the two main challenges in the area: \acl{VU} and \acl{VM}.

\subsection{Visual understanding}

One branch of studies aims at extracting better features to improve the \acl{VU} of fashion items.

For instance, \citet{Iwata2011} propose a recommender system for clothing coordinates using full-body photographs from fashion magazines.
They extract visual features, such as color, texture and local descriptors such as \ac{SIFT}, and use a probabilistic topic model for \acl{VU} of coordinates from these features.
\citet{Liu2012} target occasion-oriented clothing recommendation. 
Given a user-input event, e.g., wedding, shopping or dating, their model recommends the most suitable clothing from the user's own clothing photo album.
They adopt clothing attributes (e.g., clothing category, color, pattern) for better \acl{VU}.
\citet{Jagadeesh2014} describe a visual recommendation system for street fashion images.
They mainly focus on color modeling in terms of \acl{VU}.

The studies listed above achieve \acl{VU} mostly based on feature engineering and conventional machine learning techniques.
With the availability of large scale fashion recommendation datasets and the rapid development of deep learning models, several recent publications turn to neural networks for fashion recommendation.
\acp{CNN} are certainly widely employed \cite{McAuley2015,Li2017MiningFO}.
\citet{DBLP:conf/aaai/MaJZFLT17} build a taxonomy based on a theory of aesthetics to describe aesthetic features of fashion items quantitatively and universally. 
Then they capture the internal correlation in clothing collocations by a novel fashion-oriented multi-modal deep learning based model.
\citet{Song2017} use a pre-trained \ac{CNN} on ImageNet to extract visual features.
Then, to improve \acl{VU} with contextual information (such as titles and categories), they propose to use multi-modal auto-encoders to exploit the latent compatibility of visual and contextual features.
\citet{Han2017} enrich \acl{VU} by incorporating sequential information by using a \ac{Bi-LSTM} to predict the next item conditioned on previous ones. 
They further inject attribute and category information as a kind of regularization to learn a visual-semantic space by regressing visual features to their semantic representations. 
\citet{DBLP:conf/icdm/KangFWM17} use a \ac{CNN}-F~\cite{Chatfield14} to learn image representations and train a personalized fashion recommendation system jointly.
Besides, they devise a personalized fashion design system based on the learned \ac{CNN}-F and user representations.
\citet{Yu:2018:ACR:3178876.3186146} propose to introduce aesthetic information into fashion recommendation. 
To achieve this, they extract aesthetic features using a pre-trained  brain-inspired deep structure on the aesthetic assessment task.
\citet{2018arXiv180608977L} enhance \acl{VU} by jointly modeling fashion recommendation and user comment generation, where the visual features learned with a \ac{CNN} are enriched because they are related to the generation of user comments.

Even though there is a growing number of studies on better \acl{VU} for fashion recommendation, none of them takes fashion generation into account like we do in this paper.

\subsection{Visual matching}

Early studies into \acl{VM} are based on conventional machine learning methods. 
\citet{Iwata2011} use a topic model to learn the relation between photographs and recommend a bottom that has the closest topic proportions to those of the given top. 
\citet{Liu2012} employ an \ac{SVM} for recommendation, which has a term describing the relationship between visual features and attributes of tops and bottoms. 
\citet{7298688} predict the popularity of an outfit to implicitly learn its compatibility by a \ac{CRF} model. 
\citet{McAuley2015} measure the compatibility between clothes by learning a distance metric with pre-trained \ac{CNN} features. 
\citet{Hu2015} propose a functional pairwise interaction tensor factorization method to model the interactions between fashion items of different categories. 
\citet{Hsiao2017Creating} develop a submodular objective function to capture the key ingredients of visual compatibility in outfits. 
They propose a topic model namely \ac{CTM} to generate compatible outfits learned from unlabeled images of people wearing outfits.

Recently, deep learning methods have been used widely in the fashion recommendation community. 
\citet{Veit2015Learning} train an end-to-end Siamese \ac{CNN} network to learn a feature transformation from images to a latent compatibility space. 
\citet{Oramas2016Modeling} mine mid-level elements from \acp{CNN} to model the compatibility of clothes.
\citet{Li2017MiningFO} use a \ac{RNN} to predict whether an outfit is popular, which also implicitly learns the compatibility relation between fashion items. 
\citet{Han2017} further train a \ac{Bi-LSTM} to sequentially predict the next item conditioned on the previous ones for learning their compatibility relationship. 
\citet{Song2017} employ a dual auto-encoder network to learn the latent compatibility space where they use the \ac{BPR} model to jointly model the relation between visual and contextual modalities and implicit preferences among fashion items. 
\citet{Song2018Neural} consider the knowledge about clothing matching and follow a teacher-student scheme to encode the fashion domain knowledge in a traditional neural network. 
And they  introduce an attentive scheme to the knowledge distillation procedure to flexibly assign rule confidence. 
\citet{Nakamura2018Outfit} present an architecture containing three subnetworks, i.e., VSE (Visual-Semantic Embedding), \ac{Bi-LSTM} and SE (Style Embedding) modules, to model the matching relation between different items to generate outfits. 
\citet{2018arXiv180608977L} propose a mutual attention mechanism into \acp{CNN} to model the compatibility between different parts of images of fashion items. 

Although there are many studies on improving \acl{VM}, none of them considers connecting it with fashion generation.
