% !TEX root = ./www2019-fp-yujie-pengjie.tex

\section{Introduction}

Fashion recommendation has attracted increasing attention~\cite{Hu2015,Jaradat:2017:DCF:3109859.3109861,DBLP:conf/icdm/KangFWM17} for its potentially wide applications in fashion-oriented online communities such as, e.g., Polyvore\footnote{\url{http://www.polyvore.com/}} and Chictopia.\footnote{\url{http://www.chictopia.com/}}
By recommending fashionable items that people may be interested in, fashion recommendation can promote the development of online retail by stimulating people's interests and participation in online shopping.
In this paper, we target \emph{outfit recommendation}, that is, given a top (i.e., upper garment), we need to recommend a list of bottoms (e.g., trousers or skirts) from a large collection that best match the top, and vice versa. 
Specifically, we allow users to provide some descriptions as conditions that the recommended items should accord with as much as possible. 

Unlike conventional recommendation tasks, outfit recommendation faces two main challenges: \emph{visual understanding} and \emph{visual matching}.
Visual understanding aims to extract effective features by building a deep understanding of fashion item images. Visual matching requires modeling a human notion of the compatibility between fashion items \cite{Song2017}, which involves matching features such as color and shape etc.
Early studies into outfit recommendation rely on feature engineering for \acl{VU} and traditional machine learning  for \acl{VM} \cite{Jagadeesh2014}.
For example, \citet{Iwata2011} define three types of feature, i.e., color, texture and local descriptors such as \ac{SIFT} (for \acl{VU}), and propose a recommendation model based on \ac{GM} (for \acl{VM}).
\citet{Liu2012} define five types of feature including \ac{HOG} \cite{Dalal:2005:HOG:1068507.1069007}, \ac{LBP} \cite{Ahonen:2006:FDL:1175897.1176245}, color moment, color histogram and skin descriptor \cite{DBLP:conf/iccv/BourdevMM11} (for \acl{VU}), and propose a latent \ac{SVM} based recommendation model (for \acl{VM}).

Recently, neural networks have been applied to address the challenges of fashion recommendation: \citet{Song2017} use a pre-trained \ac{CNN} (on ImageNet) to extract visual features (for \acl{VU}).
Then, they employ a separate \ac{BPR} \cite{Rendle2009BPR} method to exploit pairwise preferences between tops and bottoms (for \acl{VM}).
\citet{2018arXiv180608977L} propose to train feature extraction (for \acl{VU}) and preference prediction (for \acl{VM}) in a single back-propagation scheme.
They introduce a mutual attention mechanism into \ac{CNN} to improve feature extraction.
The visual features captured by these methods only describe basic characteristics (e.g., color, texture, shape) of the input items, which lack aesthetic characteristics (e.g., style, design) to describe the output items (to be recommended).
Visual understanding and matching are conducted based on recommendation loss alone, where the supervision signal is just whether two given items are matched or not and no supervision is available to directly connect the visual signals of the fashion items.
Recently, some studies have realized the importance of modeling aesthetic information.
For example, \citet{DBLP:conf/aaai/MaJZFLT17} build a universal taxonomy to quantitatively describe aesthetic characteristics of clothing.
\citet{Yu:2018:ACR:3178876.3186146} propose to encode aesthetic information by pre-training models on aesthetic assessment datasets.
However, none of them is for outfit recommendation and none improves visual understanding and matching like we do.

In this paper, we address the challenges of outfit recommendation from a novel perspective by proposing a neural co-supervision learning framework, called \acfi{FARM}.
\ac{FARM} enhances \acl{VU} and \acl{VM} with the \emph{joint supervision} of generation and recommendation learning. 
Let us explain.
By incorporating the generation process as a supervision signal, \ac{FARM} is able to encode more aesthetic characteristics, based on which we can directly generate the output items.
\ac{FARM} enhances \acl{VM} by incorporating a novel layer-to-layer matching mechanism to evaluate the matching score of generated and candidate items at different neural layers; in this manner \ac{FARM} fuses the generation features from different visual levels to improve the recommendation performance.
This layer-to-layer matching mechanism also ensures that \ac{FARM} avoids paying too much attention to the generation quality and ignoring the recommendation performance.
To the best of our knowledge, \ac{FARM} is the first end-to-end learning framework that improves outfit recommendation with joint modeling of fashion generation.

Extensive experimental results conducted on two publicly available datasets show that \ac{FARM} outperforms  state-of-the-art models on outfit recommendation, in terms of AUC and MRR.
To further demonstrate the advantages of \ac{FARM}, we conduct several analyses and case studies.

To sum up, our contributions can be summarized as follows:

\begin{itemize}[nosep,leftmargin=*]
\item We propose a neural co-supervision learning framework, \ac{FARM}, for outfit recommendation that simultaneously yields recommendation and generation.
\item We propose a layer-to-layer matching mechanism that acts as a bridge between generation and recommendation, and improves recommendation by leveraging generation features.
\item Our proposed approach is shown to be effective in experiments on two large-scale datasets.
\end{itemize}







 