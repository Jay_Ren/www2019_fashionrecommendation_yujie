% !TEX root = ./www2019-fp-yujie-pengjie.tex

\section{Experimental Setup}
We set up a series of experiments to evaluate the recommendation performance of \ac{FARM}.
Details of our experimental settings are listed below.
All code and data used to run the experiments in this paper are available at \url{https://bitbucket.org/Jay_Ren/www2019_fashionrecommendation_yujie/src/master/farm/}.

\subsection{Datasets}

Existing fashion datasets include \textit{WoW}~\cite{Liu2012}, \textit{Exact Street2Shop}~\cite{7410739}, \textit{Fashion-136K}~\cite{Jagadeesh2014}, \textit{FashionVC}~\cite{Song2017} and \textit{ExpFashion}~\cite{2018arXiv180608977L}.
\textit{WoW}, \textit{Exact Street2Shop}, and \textit{Fashion-136K} have been collected from street photos\footnote{\url{http://www.tamaraberg.com/street2shop/}} on the web and involve (visual) parsing of clothing, which still remains a great challenge in the computer vision domain \cite{Yamaguchi2012,6888484,Song2017} and which is beyond the scope of this paper.
\textit{FashionVC} and \textit{ExpFashion} have been collected from the fashion-oriented online community Polyvore\footnote{\url{http://www.polyvore.com/}} and contain both images and texts.
The images are of good quality and the texts include descriptions like names and categories.
For our experiments, we choose \textit{FashionVC} and \textit{ExpFashion}.
The statistics of the two datasets are given in Table~\ref{dataset}.
\begin{table}[h]
\small
\centering
\caption{Dataset statistics.}
\label{dataset}
\begin{tabular}{lrrr}
\toprule
\bf Dataset	& \bf Tops	& \bf Bottoms	& \bf Outfits\\
\midrule
FashionVC~\cite{Song2017}	& 14,871 & 13,663  & 20,726\\
ExpFashion~\cite{2018arXiv180608977L}	& 168,682 & 117,668  & 853,991\\
\bottomrule
\end{tabular}
\end{table}
We preprocess \textit{FashionVC} or \textit{ExpFashion} with the following steps, taking
bottom recommendation as an example.
For each tuple $(\mathit{top}, \mathit{top}\ \mathit{description}, \mathit{bottom}, \mathit{bottom}\ \mathit{description})$, we regard $(\mathit{top}, \mathit{bottom}\ \mathit{description})$ as input and the \emph{bottom} as the ground truth output.
We follow existing studies~\cite{Song2017} and randomly select bottoms to generate 100 candidates along with the ground truth bottoms in the validation and test set.
Similar processing steps are used for top recommendation.

\subsection{Implementation details}
The parameters $W$, $H$, $D$ and $N$ of the encoder and the generator are set to 1, 1, 1024 and 1024, respectively.
The size $e$ of the visual semantic word embedding, the semantic representation and the visual representation is set to 100.
And the latent variable size $k$ is set to 100 too.
The 7th, the 6th and the 5th layers of the encoder \ac{CNN} are adopted to compute the layer-to-layer matching with the input, the 1st and the 2nd layers of the generator \ac{DCNN}.
To build descriptions, we first filter out words whose frequency is less than 100.
Then, we manually go through the rest to only keep words that can describe tops or bottoms.
Finally, the remaining vocabulary size $D_d$ is 547.
During training, we initialize model parameters randomly with the Xavier method~\citep{Glorot2010Understanding}.
We choose Adam~\citep{Kingma2014Adam} as our optimization algorithm.
For the hyper-parameters of the Adam optimizer, we set the learning rate $\alpha$ = 0.001, two momentum parameters $\beta$1 = 0.9 and $\beta$2 = 0.999, and $\epsilon$ = $10^{-8}$.
We apply dropout \cite{Srivastava2014Dropout} to the output of our encoder and set the rate to 0.5.
We also apply gradient clipping \cite{Pascanu2013} with range $[-5, 5]$ during training.
We use a mini-batch size 64 by grid search to both speed up the training and converge quickly.
We test the model performance on the validation set for every epoch.
Our framework is implemented with MXNet~\cite{Chen2015MXNet}.
All experiments are conducted on a single Titan X GPU.

\subsection{Methods used for comparison}
We choose the following methods for comparison.

\begin{itemize}[nosep,leftmargin=*]
\item \textit{LR}: Logistic Regression (LR) is a standard machine learning method~\cite{James2013An}.
We use it to predict whether a candidate bottom matches a given $(\mathit{top},\mathit{bottom}\ \mathit{description})$ pair or not.
Specifically, we employ a pre-trained \ac{CNN} to extract visual features from images.
Then we follow Eq.~\ref{LR} to calculate the matching probability $p$:
\begin{equation}
\label{LR}
p = \mathrm{sigmoid}(\textbf{w}_t^T\textbf{v}_t+\textbf{w}_b^T\textbf{v}_b+\textbf{w}_d^T\textbf{d}),
\end{equation}
where $\textbf{v}_t$ and $\textbf{v}_b\in\mathbb{R}^{D_v}$ are the visual features of the top and the bottom respectively, $\textbf{w}_t$ and $\textbf{w}_b\in\mathbb{R}^{D_v}$, and $\textbf{w}_d\in\mathbb{R}^{D_d}$.
$D_v$ is set to 4096 in our experiments.

\item \textit{IBR}$_d$: IBR~\cite{McAuley2015} learns a visual style space in which related objects are close and unrelated objects are far.
In order to consider the given descriptions at the same time, we modify IBR by projecting descriptions to the visual style space.
As a result, we can evaluate the matching degree between objects and descriptions by their distance in the space.
Specifically, the distance function between the candidate bottom $b$ and the given $(\mathit{top},\mathit{bottom}\ \mathit{description})$ pair $(t,d)$ is as follows:
\begin{equation}
m_{tdb} = \|\textbf{W}_v\textbf{v}_t-\textbf{W}_v\textbf{v}_b\|^2_2+\|\textbf{W}_d\textbf{v}_d-\textbf{W}_v\textbf{d}\|^2_2,
\end{equation}
where $\textbf{W}_v\in\mathbb{R}^{K\times{D_v}}$, $\textbf{W}_d\in\mathbb{R}^{K\times{D_d}}$, $\textbf{v}_t$ and $\textbf{v}_b\in\mathbb{R}^{D_v}$ are the visual features extracted by a pre-trained \ac{CNN}, and $K$ is the dimension of the visual style space.
$D_v$ is 4096, and $K$ is 100 in our experiments.
We refer to the modified version as IBR$_d$.

\item \textit{BPR-DAE}$_d$: BPR-DAE~\cite{Song2017} can jointly model the implicit matching preference between items in visual and textual modalities and the coherence relation between different modalities of items.
In our task, we do not have other text information except descriptions, so we first remove the part of BPR-DAE that is related to text information.
Then, for evaluating the matching score between the given description and the candidate item, we project the description representation and the item representation to the same latent space:
\begin{equation}
\textbf{v}_d' = \mathrm{sigmoid}(\textbf{W}_d\textbf{d}),\quad 
\textbf{v}_i' = \mathrm{sigmoid}(\textbf{W}_v\textbf{v}_i),
\end{equation}
where $\textbf{W}_d\in\mathbb{R}^{K\times{D_d}}$, $\textbf{W}_v\in\mathbb{R}^{K\times{D_v}}$, and $\textbf{v}_i\in\mathbb{R}^{D_v}$ is the latent representation of item $i$ learned by BPR-DAE.
Finally, we follow Eq.~\ref{bpr-dae} to evaluate the compatibility between a candidate bottom $b$ and a given $(\mathit{top},\mathit{bottom}\ \mathit{description})$ pair $(t,d)$:
\begin{equation}
\label{bpr-dae}
m_{tdb} = \textbf{v}_t^T\textbf{v}_b+\textbf{v}_d'^T\textbf{v}_b'.
\end{equation}
We set $D_v = 512$, and $K = 100$ in experiments.
We refer to the modified version as BPR-DAE$_d$.

\item \textit{DVBPR}$_d$: DVBPR~\cite{DBLP:conf/icdm/KangFWM17} learns the image representations and trains the recommender system jointly to recommend fashion items for users.
We adopt DVBPR to our task and refer to it as DVBPR$_d$.
Specifically, we first follow DVBPR to use a \ac{CNN}-F to learn image representations of tops and bottoms.
Then we calculate the matching score between a bottom and the given $(\mathit{top}, \mathit{bottom}$ $\mathit{description})$ pair by Eq.~\ref{dvbpr}:
\begin{equation}
\label{dvbpr}
m_{tdb} = \textbf{v}_t^T\textbf{v}_b+\textbf{v}_d^T\textbf{v}_b,
\end{equation}
where $\textbf{v}_t$ and $\textbf{v}_b\in\mathbb{R}^K$ are the image representations of the top and bottom respectively, $\textbf{v}_d\in\mathbb{R}^K$ is the description representation learned in the same way as \ac{FARM}, and $K$ is set to 100 in experiments.
\end{itemize}

\subsection{Evaluation metrics}
We employ \acfi{MRR} and \acfi{AUC} to evaluate the recommendation performance, which are widely used in recommender systems~\cite{Rendle2010,Zhang2013,DBLP:conf/cikm/LiRCRLM17}.

In the case of bottom recommendations, for example, MRR and AUC are calculated as follows:
\begin{equation}
\text{MRR}=\frac{1}{|\mathcal{Q}^{td}|}\sum_{i=1}^{|\mathcal{Q}^{td}|} \frac{1}{\mathit{rank}_i},
\end{equation}
where $\mathcal{Q}^{td}$ is the $(\mathit{top}, \mathit{bottom}\ \mathit{description})$ collection as queries, and $\mathit{rank}_i$ refers to the rank position of the first positive bottom for the $i$-th $(\mathit{top},\mathit{bottom}\ \mathit{description})$ pair. 
Furthermore,
\begin{equation}
\text{AUC}=\frac{1}{|\mathcal{Q}^{td}|} \sum_{(t,d)\in{\mathcal{Q}^{td}}} \frac{1}{|E(t,d)|}\sum_{(p,n)\in{E(t,d)}}\delta(s_p > s_n),
\end{equation}
where $E(t,d)$ is the set of all positive and negative candidate bottoms for the given top $t$ and the given bottom description $d$, $s_p$ is the matching score of a positive bottom $p$, $s_n$ is the matching score of a negative bottom $n$, and $\delta(\alpha)$ is an indicator function that equals 1 if $\alpha$ is true and 0 otherwise.
