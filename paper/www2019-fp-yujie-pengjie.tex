%%%% Proceedings format for most of ACM conferences (with the exceptions listed below) and all ICPS volumes.
\documentclass[sigconf]{acmart}

\copyrightyear{2019}
\acmYear{2019} 
\setcopyright{iw3c2w3}
\acmConference[WWW '19]{Proceedings of the 2019 World Wide Web Conference}{May 13--17, 2019}{San Francisco, CA, USA}
\acmBooktitle{Proceedings of the 2019 World Wide Web Conference (WWW '19), May 13--17, 2019, San Francisco, CA, USA}
\acmPrice{}
\acmDOI{10.1145/3308558.3313614}
\acmISBN{978-1-4503-6674-8/19/05}
\fancyhead{}

\usepackage{natbib}
\usepackage{url}
\usepackage{booktabs} % For formal tables
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{amssymb}
\usepackage{array}
\usepackage[normalem]{ulem}
\usepackage{footmisc}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{multirow}
\usepackage{enumitem}
\usepackage[skip=0pt]{caption}
\usepackage{subfigure}
\usepackage{xcolor}
\usepackage{pifont}
\usepackage{bm}
\usepackage{acronym}
\acrodef{CNN}{Convolutional Neural Network}
\acrodef{DCNN}{DeConvolutional Neural Network}
\acrodef{RNN}{Recurrent Neural Network}
\acrodef{Bi-LSTM}{Bidirectional Long Short-Term Memory Network}
\acrodef{BPR}{Bayesian Personalized Ranking}
\acrodef{ELBO}{Evidence Lower BOund}
\acrodef{AUC}{Area Under the ROC Curve}
\acrodef{MRR}{Mean Reciprocal Rank}
\acrodef{VU}{visual understanding}
\acrodef{VM}{visual matching}
\acrodef{SIFT}{Scale Invariant Feature Transform}
\acrodef{HOG}{Histograms of Oriented Gradient}
\acrodef{LBP}{Local Binary Pattern}
\acrodef{SVM}{Support Vector Machine}
\acrodef{GM}{Graphical Models}
\acrodef{CRF}{Conditional Random Field}
\acrodef{CTM}{Correlated Topic Models}
\acrodef{VAE}{Variational Auto-Encoder}
\acrodef{FARM}{FAshion Recommendation Machine}
\newcount\colveccount
\newcommand*\colvec[1]{
        \global\colveccount#1
        \begin{matrix}
        \colvecnext
}
\def\colvecnext#1{
        #1
        \global\advance\colveccount-1
        \ifnum\colveccount>0
                \\
                \expandafter\colvecnext
        \else
                \end{matrix}
        \fi
}
\newcommand{\argmax}{\mathop{\mathrm{arg}\,\max}}
\newcommand{\remove}[1]{\sout{\textcolor{orange}{#1}}}
\newcommand{\todo}[1]{\textcolor{red}{#1}}
\newcommand{\changed}[1]{\textcolor{blue}{#1}}
\newcommand{\thindagger}{\rlap{$^\dagger$}}
\newcommand{\onedigit}{\phantom{\textbf{1}}}
\newcommand{\LYJ}[1]{\textcolor{green}{\textbf{#1}}}
\newcommand{\RPJ}[1]{\textcolor{blue}{\textbf{#1}}}
\newcommand{\change}[1]{\textcolor{yellow}{\textbf{#1}}}

% Copyright
%\setcopyright{iw3c2w3}

% DOI
%\acmDOI{10.475/123_4}

% ISBN
%\acmISBN{123-4567-24-567/08/06}

%Conference
%\acmConference[WWW 2019]{The Web Conference 2019:  The 28th International World Wide Web Conference}{May 13-17, 2019}{San Francisco}
%\acmYear{2019}
%\copyrightyear{2019}

%\acmArticle{4}
%\acmPrice{15.00}

%\fancyhead[L]{}
%\fancyhead[R]{TheWebConf'19, May 2019, San Francisco, CA USA}

\begin{document}
\title{Improving Outfit Recommendation with Co-supervision of Fashion Generation}

\author{Yujie Lin}
\authornote{Co-first author.}
\orcid{}
\affiliation{%
\institution{Shandong University}
\city{Jinan}
\country{China}
}
\email{yu.jie.lin@outlook.com}

\author{Pengjie Ren}
\authornotemark[1]
\orcid{}
\affiliation{%
\institution{University of Amsterdam}
\city{Amsterdam}
\country{The Netherlands}
}
\email{p.ren@uva.nl}

\author{Zhumin Chen}
\orcid{}
\affiliation{%
\institution{Shandong University}
\city{Jinan}
\country{China}
}
\email{chenzhumin@sdu.edu.cn}

\author{Zhaochun Ren}
\orcid{}
\affiliation{%
\institution{Shandong University}
\city{Jinan}
\country{China}
}
\email{zhaochun.ren@sdu.edu.cn}

\author{Jun Ma}
\orcid{}
\affiliation{%
\institution{Shandong University}
\city{Jinan}
\country{China}
}
\email{majun@sdu.edu.cn}

\author{Maarten de Rijke}
\orcid{0000-0002-1086-0202}
\affiliation{%
\institution{University of Amsterdam}
\city{Amsterdam}
\country{The Netherlands}
}
\email{derijke@uva.nl}

\begin{abstract}
The task of fashion recommendation includes two main challenges: \emph{visual understanding} and \emph{visual matching}.
Visual understanding aims to extract effective visual features.
Visual matching aims to model a human notion of compatibility to compute a match between fashion items.
Most previous studies rely on recommendation loss alone to guide visual understanding and matching.
Although the features captured by these methods describe basic characteristics (e.g., color, texture, shape) of the input items, they are not directly related to the visual signals of the output items (to be recommended).
This is problematic because the aesthetic characteristics (e.g., style, design), based on which we can directly infer the output items, are lacking.
Features are learned under the recommendation loss alone, where the supervision signal is simply whether the given two items are matched or not.

To address this problem, we propose a neural co-supervision learning framework, called the \ac{FARM}.
\ac{FARM} improves \acl{VU} by incorporating the supervision of generation loss, which we hypothesize to be able to better encode aesthetic information.
\ac{FARM} enhances \acl{VM} by introducing a novel layer-to-layer matching mechanism to fuse aesthetic information more effectively, and meanwhile avoiding paying too much attention to the generation quality and ignoring the recommendation performance.

Extensive experiments on two publicly available datasets show that \ac{FARM} outperforms state-of-the-art models on outfit recommendation, in terms of AUC and MRR.
Detailed analyses of generated and recommended items demonstrate that \ac{FARM} can encode better features and generate high quality images as references to improve recommendation performance.
\end{abstract}
%
% The code below should be generated by the tool at
% http://dl.acm.org/ccs.cfm
% Please copy and paste the code instead of the example below.
%
%Please go to: http://dl.acm.org/ccs_flat.cfm and choose as many classifiers as you feel appropriate to your submission and the level of relevance: high, medium, low. Then click "[continue]"to choose additional classifiers. When you are done choosing your choices of classifiers, click on the link near the top of the pop-up window "[View CCS TeX Code]"
\begin{CCSXML}
<ccs2012>
<concept>
<concept_id>10002951.10003317.10003347.10003350</concept_id>
<concept_desc>Information systems~Recommender systems</concept_desc>
<concept_significance>500</concept_significance>
</concept>
</ccs2012>
\end{CCSXML}

\ccsdesc[500]{Information systems~Recommender systems}

\keywords{Outfit matching; Fashion generation; Fashion recommendation}

\maketitle

\input{01-introduction}
\input{02-relatedwork}
\input{03-method}
\input{04-experimental-setup}
\input{05-result}
\input{06-analysis}
\input{07-conclusion}

\section*{Acknowledgments}
We thank the anonymous reviewers for their helpful comments. 

This work is supported by the Natural Science Foundation of China (61672324, 61672322), the Natural Science Foundation of Shandong province (2016ZRE27468), the Fundamental Research Funds of Shandong University, Ahold Delhaize, the Association of Universities in the Netherlands, and the Innovation Center for Artificial Intelligence (ICAI).
All content represents the opinion of the authors, which is not necessarily shared or endorsed by their respective employers and/or sponsors.

\newpage

\bibliographystyle{ACM-Reference-Format}
\bibliography{www2019-fp-yujie-pengjie}

\end{document}
