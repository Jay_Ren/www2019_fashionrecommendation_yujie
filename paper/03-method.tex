% !TEX root = ./www2019-fp-yujie-pengjie.tex

\section{Neural Fashion Recommendation}
\begin{figure*}
 \centering
 \subfigure[Encoder]{
 \label{f_3_2a}
 \includegraphics[width=0.8\textwidth]{fig/3-2.eps}}
 \subfigure[Generator]{
 \label{f_3_2b}
 \includegraphics[width=1.0\textwidth]{fig/3-3.eps}}
 \caption{Details of the encoder and the generator in \ac{FARM}, where $k$ represents kernel size, $n$ represents the number of channels, $s$ represents strides and $p$ represents padding.}
 \label{f_3_2}
\end{figure*}

\subsection{Overview}
Given a top $t$ from a pool $\mathcal{T} = \{t_1, t_2, \ldots , t_{N_t}\}$ and a user's description $d$ for the target bottom, the \emph{bottom recommendation task} is to recommend a list of bottoms from a candidate pool $\mathcal{B} = \{b_1, b_2, \ldots , b_{N_b}\}$.
Similarly, the \emph{top recommendation task} is to recommend a ranked list of tops for a given bottom and top description pair.
Here, we use bottom recommendation as the setup to introduce our framework \ac{FARM}.

As shown in Figure~\ref{f_3_1}, \ac{FARM} consists of two parts, i.e., a fashion generator (for \acl{VU}) and a fashion recommender (for \acl{VM}), where the fashion generator is actually an auxiliary module for recommendation.
For the fashion generator, we use a \ac{CNN} as the top encoder to extract the visual features from a given top image $\textbf{I}_t$.
We learn the semantic representation for the bag-of-words vector $\textbf{d}$ of a given bottom description.
Then we use a variational transformer to learn the mapping from the bottom distribution to a specific Gaussian distribution that is based on the visual features of $\textbf{I}_t$ and the semantic representation of $\textbf{d}$.
Finally, we sample a random vector from the Gaussian distribution and input it to a \ac{DCNN}~\cite{Zeiler2011Adaptive} (as bottom generator) to generate a bottom image $\textbf{I}_g$ that matches $\textbf{I}_t$ and $\textbf{d}$, which explicitly forces the top encoder to encode more aesthetic matching information into the visual features.
For the fashion recommender, we also employ a \ac{CNN} as the bottom encoder to extract the visual features from a candidate bottom image $\textbf{I}_b$.
Then we evaluate the matching score between $\textbf{I}_b$ and $(\textbf{I}_t, \textbf{d})$ pair from three angles, namely the visual matching between $\textbf{I}_b$ and $\textbf{I}_t$, the description matching between $\textbf{I}_b$ and $\textbf{d}$, and the layer-to-layer matching between $\textbf{I}_b$ and $\textbf{I}_g$ which leverages the generation information to improve the recommendation.
\ac{FARM} jointly trains the fashion generator and fashion recommender.
Next we will detail each of these two main parts.

\subsection{Fashion generator}
Given an image $\textbf{I}_t$ of a top $t$ and the bag-of-words vector $\textbf{d}$ of a bottom description $d$, the fashion generator needs to generate a bottom image $\textbf{I}_g$ that not only matches $\textbf{I}_t$, but also meets $\textbf{d}$ as much as possible.
We enforce the extracted visual features from $\textbf{I}_t$ to contain the information about its matching bottom by using the generator as a supervision signal.
The generated image can be seen as a reference for recommendation.

Specifically, for a generated bottom image $\textbf{I}_g$ that matches $\textbf{I}_t$ and $\textbf{d}$, the aim of the fashion generator is to maximize Eq.~\ref{p(I_g|I_t,d)}:
\begin{equation}
\label{p(I_g|I_t,d)}
p(\textbf{I}_g|\textbf{I}_t,\textbf{d}) = \int_{\textbf{z}}p(\textbf{I}_g|\textbf{z},\textbf{I}_t,\textbf{d})p(\textbf{z}|\textbf{I}_t,\textbf{d})\mathrm{d}\textbf{z},
\end{equation}
where $p(\textbf{z}|\textbf{I}_t,\textbf{d})$ is the top encoder, $p(\textbf{I}_g|\textbf{z},\textbf{I}_t,\textbf{d})$ is the bottom generator, and $\textbf{z}$ is the latent variable.
Because the integral of the marginal likelihood shown in Eq.~\ref{p(I_g|I_t,d)} is intractable, inspired by variational inference~\cite{Blei2017Variational}, we first find the \ac{ELBO} of $p(\textbf{I}_g|\textbf{I}_t,\textbf{d})$, as shown in Eq.~\ref{ELBO}:
\begin{eqnarray}
\label{ELBO}
\mathrm{ELBO} = \mathbb{E}_{\textbf{z}\sim{q(\textbf{z}|\textbf{I}_t,\textbf{d})}}[\log{p(\textbf{I}_g|\textbf{z},\textbf{I}_t,\textbf{d})}]\nonumber\\
-\mathrm{KL}[q(\textbf{z}|\textbf{I}_t,\textbf{d}){\parallel}p(\textbf{z}|\textbf{I}_t,\textbf{d})],
\end{eqnarray}
where $q(\textbf{z}|\textbf{I}_t,\textbf{d})$ is the approximation of the intractable true posterior $p(\textbf{z}|\textbf{I}_g,\textbf{I}_t,\textbf{d})$.
The following inequality holds for the \ac{ELBO}:
\begin{equation}
\log{p(\textbf{I}_g|\textbf{I}_t,\textbf{d})} \geqslant \mathrm{ELBO}.
\end{equation}
Hence, we can maximize the \ac{ELBO} so as to maximize $\log{p(\textbf{I}_g|\textbf{I}_t,\textbf{d})}$.
The \ac{ELBO} contains three components:  $q(\textbf{z}|\textbf{I}_t,\textbf{d})$, $p(\textbf{z}|\textbf{I}_t,\textbf{d})$ and $p(\textbf{I}_g|\textbf{z},\textbf{I}_t,\textbf{d})$.
Below we explain each component in detail.

\subsubsection{$q(\textbf{z}|\textbf{I}_t,\textbf{d})$ and $p(\textbf{z}|\textbf{I}_t,\textbf{d})$.}
We propose a variational transformer (as shown in Figure~\ref{f_3_1}) to model these two components, which transforms $\textbf{I}_t,\textbf{d}$ into a latent variable $\textbf{z}$.
As with previous work~\cite{Kingma2014Auto, Rezende2014Stochastic}, we assume that $q(\textbf{z}|\textbf{I}_t,\textbf{d})$ and $p(\textbf{z}|\textbf{I}_t,\textbf{d})$ are Gaussian distributions, i.e.,
\begin{equation}
q(\textbf{z}|\textbf{I}_t,\textbf{d}) \sim \mathcal{N}(\textbf{z};\bm{\mu},\bm{\sigma}^2),\quad
p(\textbf{z}|\textbf{I}_t,\textbf{d}) \sim \mathcal{N}(0,1),
\end{equation}
where $\bm{\mu}$ and $\bm{\sigma}$ denote the variational mean and standard deviation respectively, which are calculated with our top encoder and variational transformer as follows.

Specifically, for a top image $\textbf{I}_t$ of size $128\times{128}$ with $3$ channels, we first use a \ac{CNN}, i.e., \emph{the top encoder} (as shown in Figure~\ref{f_3_2a}) to extract visual features $\textbf{F}_t$:
\begin{equation}
\textbf{F}_t = \textbf{CNN}(\textbf{I}_t),
\end{equation}
where $\textbf{F}_t\in\mathbb{R}^{W\times{H}\times{D}}$, $W$ and $H$ are the width and height of the output feature maps, respectively, and $D$ is the number of output feature maps. 
And we flatten $\textbf{F}_t$ into a vector $\textbf{f}_t\in\mathbb{R}^N$, where $N = W\times{H}\times{D}$, and project $\textbf{f}_t$ to the visual representation $\textbf{v}_t$:
\begin{equation}
\textbf{v}_t = \mathrm{sigmoid}(\textbf{W}_{vt}\textbf{f}_t+\textbf{b}_{vt}),
\end{equation}
where $\textbf{W}_{vt}\in\mathbb{R}^{e\times{N}}$, $\textbf{v}_t$ and $\textbf{b}_{vt}\in\mathbb{R}^e$, and $e$ is the size of the representation. 

Besides the top image, \ac{FARM} also allows users to give a natural language description $\textbf{d}$, which describes the ideal bottom they want.
In order to take into account the description $\textbf{d}$, we follow Eq.~\ref{v_d} to get the semantic representation $\textbf{v}_d$:
\begin{equation}
\label{v_d}
\textbf{v}_d = \mathrm{sigmoid}(\textbf{W}_d\textbf{d}),
\end{equation}
where $\textbf{v}_d\in\mathbb{R}^e$, $\textbf{d}\in\mathbb{R}^{D_d}$, $D_d$ is the vocabulary size, and $\textbf{W}_d\in\mathbb{R}^{e\times{D_d}}$ is the visual semantic word embedding matrix~\cite{Nakamura2018Outfit}, which transforms words from the textual space to the visual space.
Specially, when $d$ is an empty description, $\textbf{v}_d$ is a zero vector.

Then \emph{the variational transformer} uses the visual representation $\textbf{v}_t$ and the semantic representation $\textbf{v}_d$ to calculate the mean $\bm{\mu}$ and standard deviation $\bm{\sigma}$ for $q(\textbf{z}|\textbf{I}_t,\textbf{d})$:
\begin{equation}
\begin{split}
\bm{\mu} = {} & \textbf{W}_{\mu{t}}\textbf{v}_t+\textbf{W}_{\mu{d}}\textbf{v}_d+\textbf{b}_{\mu} \\
\log{\bm{\sigma}^2} = {} & \textbf{W}_{\sigma{t}}\textbf{v}_t+\textbf{W}_{\sigma{d}}\textbf{v}_d+\textbf{b}_{\sigma},
\end{split}
\end{equation}
where $\textbf{W}_{\mu{t}}$, $\textbf{W}_{\mu{d}}$, $\textbf{W}_{\sigma{t}}$ and $\textbf{W}_{\sigma{d}}\in\mathbb{R}^{k\times{e}}$, $\bm{\mu}$, $\bm{\sigma}$, $\textbf{b}_{\mu}$ and $\textbf{b}_{\sigma}\in\mathbb{R}^k$, and $k$ is the size of latent variable $\textbf{z}$.
The latent variable $\textbf{z}$ can be calculated by the reparameterization trick~\cite{Kingma2014Auto, Rezende2014Stochastic}:
\begin{equation}
\bm{\epsilon}\sim\mathcal{N}(0,1),\quad 
\textbf{z} = \bm{\mu}+\bm{\sigma}\otimes\bm{\epsilon},
\end{equation}
where $\bm{\epsilon}$ and $\textbf{z}\in\mathbb{R}^k$, and $\bm{\epsilon}$ is the auxiliary noise variable.
By the reparameterization trick, we make sure $\textbf{z}$ is a random vector sampled from $\mathcal{N}(\textbf{z};\bm{\mu},\bm{\sigma}^2)$.

\subsubsection{$p(\textbf{I}_g|\textbf{z},\textbf{I}_t,\textbf{d})$.}
We use the bottom generator (as shown in Figure~\ref{f_3_2b}) to generate $\textbf{I}_g$ from the variable $\textbf{z}$.
We also assume $p(\textbf{I}_g|\textbf{z},\textbf{I}_t,\textbf{d})$ is a Gaussian distribution~\cite{Kingma2014Auto, Rezende2014Stochastic}, i.e.,
\begin{equation}
p(\textbf{I}_g|\textbf{z},\textbf{I}_t,\textbf{d}) \sim \mathcal{N}(g(\textbf{z},\textbf{I}_t,\textbf{d}),\bm{\sigma}^2),
\end{equation}
where $g$ is the bottom generator.
% and we \todo{HUH: hope} $g(\textbf{z},\textbf{I}_t,\textbf{d})=\textbf{I}_g^*$.

Specifically, we first follow Eq.~\ref{f_g} to obtain the basic visual feature vector $\textbf{f}_g$:
\begin{equation}
\label{f_g}
\textbf{f}_g = \mathrm{relu}(\textbf{W}_{gz}\textbf{z}+\textbf{W}_{gt}\textbf{v}_t+\textbf{W}_{gd}\textbf{v}_d+\textbf{b}_g),
\end{equation}
where $\textbf{f}_g$ and $\textbf{b}_g\in\mathbb{R}^N$, $\textbf{W}_{gz}\mathbb{R}^{N\times{k}}$, $\textbf{W}_{gt}$ and $\textbf{W}_{gd}\in\mathbb{R}^{N\times{e}}$.
Then we reshape $\textbf{f}_g$ into a $3$-D tensor $\textbf{F}_g\in\mathbb{R}^{W\times{H}\times{D}}$, which is the reverse operation to what we do for $\textbf{F}_t$.
Finally, we use a \ac{DCNN}, i.e., \emph{the bottom generator} to generate the bottom image $\textbf{I}_g$:
\begin{equation}
\textbf{I}_g = \textbf{DCNN}(\textbf{F}_g),
\end{equation}
where $\textbf{I}_g\in\mathbb{R}^{128\times{128}\times{3}}$. To avoid generating blurry images~\cite{Bao2017CVAE}, we divide the process of image generation into two stages~\cite{8237891, DBLP:journals/corr/CaiGJ17}.
The first stage is an ordinary deconvolutional neural network that generates low-resolution images.
The second stage is similar to the super-resolution residual network (SRResNet)~\cite{Ledig2017Photo}, which accepts the images from the first stage and refines them to generate high quality ones.
The \ac{DCNN} is meant to capture high-level aesthetic features of the bottoms to be recommended~\citep{Zeiler2011Adaptive,Zeiler2014Visualizing}.
Besides, in order to generate the bottom, the generation process also forces the top encoder to capture more aesthetic information.

During training, we first sample a $z$ from $q(\textbf{z}|\textbf{I}_t,\textbf{d})$.
Then we generate $\textbf{I}_g$ with $g(\textbf{z},\textbf{I}_t,\textbf{d})$. 
During testing, in order to avoid the randomness introduced by $\bm{\epsilon}$, we directly generate $\textbf{I}_g$ by $g(\textbf{z}=\bm{\mu},\textbf{I}_t,\textbf{d})$.

\subsection{Fashion recommender}
Given the image $\textbf{I}_b$ of a bottom $b$, the fashion recommender needs to evaluate the matching score between $\textbf{I}_b$ and the pair $(\textbf{I}_t,\textbf{d})$.
Specifically, we first use the \emph{bottom encoder} (as shown in Figure~\ref{f_3_2a}), which has the same structure as the top encoder (parameters not shared), to extract visual features $\textbf{F}_b\in\mathbb{R}^{W\times{H}\times{D}}$ from $\textbf{I}_b$.
Then we flatten $\textbf{F}_b$ into a vector $\textbf{f}_b\in\mathbb{R}^N$ and project $\textbf{f}_b$ to the visual representation $\textbf{v}_b$.
Next, we calculate the matching score between $\textbf{I}_b$ and the pair $(\textbf{I}_t,\textbf{d})$ in three ways.

\subsubsection{Visual matching}
We propose visual matching to evaluate the compatibility between $\textbf{I}_b$ and $\textbf{I}_t$ based on their visual features.
Specifically, we calculate the visual matching score $s_v$ between $\textbf{I}_b$ and $\textbf{I}_t$ by Eq.~\ref{s_v}:
\begin{equation}
\label{s_v}
s_v = \textbf{v}_b^T\textbf{v}_t.
\end{equation}

\subsubsection{Description matching}
For evaluating the matching degree between $\textbf{I}_b$ and $\textbf{d}$, we propose to match descriptions.
The description matching score $s_d$ between $\textbf{I}_b$ and $\textbf{d}$ is calculated by Eq.~\ref{s_d}:
\begin{equation}
\label{s_d}
s_d = \textbf{v}_b^T\textbf{v}_d.
\end{equation}
Note that if $d$ does not contain any word, $s_d$ equals 0.

\subsubsection{Layer-to-layer matching}
As we will demonstrate in our experiments in Section~\ref{section:pma}, a simple combination of generation and recommendation is not able to improve the recommendation performance.
The reason is that there is no direct connection between generation and recommendation, which results in two issues.
First, the aesthetic information from the generation process cannot be used effectively.
Second, the generation process might introduce features that are only helpful for generation while unhelpful for recommendation.
To overcome these issues, we propose a layer-to-layer matching mechanism.
Specifically, we denote the visual features of the $l$-th \ac{CNN} layer in the bottom encoder as $\textbf{F}^l_b\in\mathbb{R}^{W^l\times{H^l}\times{D^l}}$.
And we denote the visual features of the corresponding \ac{DCNN} layer, which has the same size as $\textbf{F}^l_b$, in the bottom generator as $\textbf{F}^l_g\in\mathbb{R}^{W^l\times{H^l}\times{D^l}}$.
Then, we reshape $\textbf{F}^l_b = [\textbf{f}^l_{b,1}, \ldots, \textbf{f}^l_{b,S}]$ by flattening the width and height of the original $\textbf{F}^l_b$, where $S=W^l\times{H^l}$ and $\textbf{f}^l_{b,i}\in\mathbb{R}^{D^l}$.
And we can consider $\textbf{f}^l_{b,i}$ as the visual features of the $i$-th location of $\textbf{I}_b$. 
We perform global-average-pooling in $\textbf{F}^l_b$ to get the global visual features $\textbf{f}^l_b\in\mathbb{R}^{D^l}$:
\begin{equation}
\textbf{f}^l_b = \frac{1}{S}\sum^S_{i=1}\textbf{f}^l_{b,i}.
\end{equation}
We project $\textbf{f}^l_b$ to the visual representation $\textbf{v}^l_b\in\mathbb{R}^e$:
\begin{equation}
\textbf{v}^l_b = \mathrm{sigmoid}(\textbf{W}^l_{vb}\textbf{f}^l_b+\textbf{b}^l_{vb}),
\end{equation}
where $\textbf{W}^l_{vb}\in\mathbb{R}^{e\times{D^l}}$ and $\textbf{b}^l_{vb}\in\mathbb{R}^e$.
The same operations apply to $\textbf{F}^l_g$ to get $\textbf{v}^l_g$.
Then we calculate the dot product between $\textbf{v}^l_b$ and $\textbf{v}^l_g$, which represents the matching degree $s^l_g$ between $\textbf{I}_b$ and $\textbf{I}_g$ in the $l$-th visual level:
\begin{equation}
\label{s^l_g}
s^l_g = {\textbf{v}^l_b}^T\textbf{v}^l_g.
\end{equation}
For different visual levels, we sum all $s^l_g$ to get the matching score $s_g$ between $\textbf{I}_b$ and $\textbf{I}_g$:
\begin{equation}
s_g = \sum_{l\in{L}}s^l_g,
\end{equation}
where $L$ is the selected \ac{CNN} layer set for layer-to-layer matching.

Finally, the total matching score $s$ between $\textbf{I}_b$ and the pair $(\textbf{I}_t,\textbf{d})$ is defined as follows:
\begin{equation}
\label{ms}
s = s_v+s_d+s_g.
\end{equation}

\subsection{Co-supervision learning framework}
For \ac{FARM}, we train the fashion generator and the fashion recommender jointly with a co-supervision learning framework.

Specifically, for the generation part, we regard the image $\textbf{I}_p$ of a positive bottom $p$, which not only matches the given top $\textbf{I}_t$ but also meets the given description $\textbf{d}$, as the generation target.
And we denote the generated bottom image in the first stage as $\textbf{I}^1_g$, and denote the generated bottom image in the second stage as $\textbf{I}^2_g$.
Then, the first loss is to maximize the first term in \ac{ELBO}, which is Eq.~\ref{L_{gen}}:
\begin{equation}
\label{L_{gen}}
\mathcal{L}_{\mathit{gen}}(t,d,p) = \frac{1}{2}\|\textbf{I}^1_g-\textbf{I}_p\|^2_2+\|\textbf{I}^2_g-\textbf{I}_p\|.
\end{equation}
The second loss is to minimize the second term in \ac{ELBO}, which is Eq.~\ref{L_{kl}}:
\begin{equation}
\label{L_{kl}}
\mathcal{L}_{\mathit{kl}}(t,d,p) = \frac{1}{2}\sum^k_{i=1}(1+\log\bm{\sigma}^2_i-\bm{\mu}^2_i-\bm{\sigma}^2_i),
\end{equation}
where $\bm{\mu}_i$ and $\bm{\sigma}_i$ are the $i$-th elements in $\bm{\mu}$ and $\bm{\sigma}$ respectively.

For the recommendation part, we employ \ac{BPR}~\cite{Rendle2009BPR} as the loss:
\begin{equation}
\label{L_{bpr}}
\mathcal{L}_{\mathit{bpr}}(t,d,p,n) = -\log(\mathrm{sigmoid}(s_p-s_n)),
\end{equation}
where $s_p$ and $s_n$ are the matching scores of a positive bottom $\textbf{I}_p$ and a negative bottom $\textbf{I}_n$, respectively (calculated with Eq.~\ref{ms}).
$\textbf{I}_n$ (image of bottom $n$) is randomly sampled.

The total loss function can be defined as follows:
\begin{equation}
\mathcal{L} = \sum_{(t,d,p,n)\in\mathcal{D}}\mathcal{L}_{\mathit{gen}}(t,d,p)+\mathcal{L}_{\mathit{kl}}(t,d,p)+\mathcal{L}_{\mathit{bpr}}(t,d,p,n),
\end{equation}
where $\mathcal{D}=\{(t,d,p,n)|t\in\mathcal{T}, d\in\mathcal{D}_b, p\in\mathcal{B}_{t,d}, n\in\mathcal{B}\setminus\mathcal{B}_{t,d}\}$, $\mathcal{D}_b$ is the bottom description set, $\mathcal{B}_{t,d}$ is the positive bottom set for the pair $(\textbf{I}_t,\textbf{d})$ and $\mathcal{B}\setminus\mathcal{B}_{t,d}$ is the negative bottom set for the pair $(\textbf{I}_t,\textbf{d})$.
The whole framework can be efficiently trained using back-propagation in an end-to-end paradigm.

For top recommendation, we follow the same way to build and train the model, but exchange the roles of tops and bottoms.
